(async function() {
    const data = await fetchJSON("/2.4.0/index.json");
    const table = document.getElementById("explorer");
    const filters = Object.keys(data.filters);
    document.getElementById("main").style.visibility = "visible";
    document.getElementById("tblock-version").innerText = data.repo.latest_version;
    document.getElementById("version").innerText = data.repo.version;
    document.getElementById("total").innerText = filters.length;
    for await(id of filters){
        let tr = document.createElement("tr");
        let td_id = document.createElement("td");
        let td_id_a = document.createElement("a");
        td_id_a.href = "view.html?id=".concat(id);
        td_id_a.innerText = id;
        if(data.filters[id].deprecated){
            td_id_a.title = "This list is deprecated";
            td_id_a.className = "deprecated";
        }
        td_id.appendChild(td_id_a);
        tr.appendChild(td_id);
        let td_title = document.createElement("td");
        td_title.innerText = data.filters[id].title;
        tr.appendChild(td_title);
        let td_description = document.createElement("td");
        td_description.innerText = data.filters[id].desc;
        tr.appendChild(td_description);
        let td_license = document.createElement("td");
        let td_license_a = document.createElement("a");
        td_license_a.href = data.filters[id].license[1];
        td_license_a.innerText = data.filters[id].license[0];
        td_license.appendChild(td_license_a);
        tr.appendChild(td_license);
        table.appendChild(tr);
    }
})();